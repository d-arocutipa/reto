import React from 'react';
import Logo from "../../assets/img/Logo.svg";
import Telefono from "../../assets/img/Telefono.svg";

const Header = () => {
    return (
        <div className="login__header--fix">
            <div className="login__header">
                <img src={Logo} alt="" className="login__logo" />
                <div className="login__parrafos">
                    <p className="login__label--grey">¿Tienes alguna duda?</p>
                    <p className="login__label--acian"><img src={Telefono} alt="" />(01) 411 6001</p>
                </div>
            </div>
        </div>
    );
}
export default Header
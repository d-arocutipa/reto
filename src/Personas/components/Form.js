import React, { useState } from 'react'
import { useForm } from "react-hook-form";
import Auto from '../../assets/img/Auto.svg'

const Form = () => {

    const { register, handleSubmit, formState: { errors } } = useForm();

    const [datos, setDatos] = useState([]);

    const onSubmit = (data, e) => {

        setDatos([
            ...datos,
            data
        ]);
       
        window.location.href="../Cotizador";

    };

    return (
        <div>
            <div className="login__formulario--izquierda">
                <div className="login__parrafo--gray">
                    <img src={Auto} alt="" className="login__formulaio--auto" />
                    <p className="login__parrafo--nuevo">¡nuevo!</p>
                    <p className="login__parrafo--principal">Seguro <span className="login__parrafo--red">Vehicular Tracking</span></p>
                    <p className="login__subtitle--gray">Cuentanos donde le haras seguimiento a tu seguro</p>
                </div>
                <p className="login__copyright">© 2021 RIMAC Seguros y Reaseguros.</p>
            </div>
            <div className="login__formulario--derecha">
                <div className="login__furmulario--login">
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <h1 className="login__titulo">Déjanos tus datos</h1>
                        <div className="login__input--grupo">
                            <div className="login__input--grupo--documento">
                                <div className="login__documento--izquierda">
                                    <select className="login__input--seleccionar" {...register('tipo_documento', { required: false})}
                                    >
                                        <option value="1">DNI</option>
                                        <option value="2">CE</option>
                                    </select>
                                    <i></i>
                                </div>
                                <div className="login__documento--derecha">
                                    <input
                                        type="number"
                                        className="login__input--text--documento" {...register('documento', { required: { value:true, message: 'Ingrese su documento'}, minLength: { value:8, message: 'Debe tener 8 digitos'} , maxLength: { value:8, message: 'Ingrese un documento valido'} })} placeholder="Nro. de documento" ></input>
                                    {errors.documento && (
                                        <span role="alert">
                                            {errors.documento.message}
                                        </span>
                                    )}
                                </div>
                            </div>
                        </div>
                        <div className="login__input--grupo">
                            <input type="number" className="login__input--text" placeholder="Celular" {...register('celular', { required: { value:true, message: 'Ingrese su celular'}, minLength: { value: 9, message: 'Ingrese un celular valido'}  })}
                            ></input>
                            {errors.celular && (
                                <span role="alert">
                                    {errors.celular.message}
                                </span>
                            )}
                        </div>
                        <div className="login__input--grupo">
                            <input type="text" className="login__input--text" placeholder="Placa" {...register('placa', { required: false })}></input>

                        </div>
                        <div className="login__input--grupo">
                            <label className="login__check">
                                Acepto la <a >Política de Protecciòn de Datos Personales</a> y los <a >Términos y Condiciones.</a>
                                <input type="checkbox" ></input>
                                <span className="login__check--control"></span>
                            </label>
                        </div>
                        <div className="login__input--grupo">
                            <button className="login__boton--cotizar" type="submit" >COTÍZALO</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default Form
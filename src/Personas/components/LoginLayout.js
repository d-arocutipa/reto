import "../../scss/app.scss";
import React from 'react';
import Header from './Header';
import Form from './Form';


const LoginLayout = () => {
    return (
        <>
            <Header />
            <Form />
        </>
    );
}

export default LoginLayout
import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import LoginLayout from '../Personas/components/LoginLayout'
import CotizadorLayout from '../Cotizador/components/CotizadorLayout';

const Routes = () => {
return(
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={LoginLayout}  />
            <Route exact path="/Cotizador" component={CotizadorLayout}  />
        </Switch>
    </BrowserRouter>
);
}

export default Routes;
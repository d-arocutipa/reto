import React, { useState } from 'react'
import Barra from "../../assets/img/Barra.svg";
import Atras from "../../assets/img/Atras.svg";
import Personal from "../../assets/img/Personal.svg";


const Form = () => {
    return (
        <div>
            <div className="cotizacion__formulario--izquierda">
                <div className="cotizador__pasos">
                    <div>
                        <div className="cotizador__paso">
                            <div className="cotizador__numero--1">
                                <span >1</span>
                                <p className="cotizador__numero--parrafo">Datos</p>
                            </div>
                        </div>
                    </div>
                    <div className="cotizador__barra"><img src={Barra} />
                    </div>
                    <div>
                        <div className="cotizador__paso">
                            <div className="cotizador__numero">
                                <span>2</span>
                                <p className="cotizador__numero--parrafo">Arma tu plan</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="login__formulario--derecha">
                <div className="login__formulario">
                    <a href="/"> <div className="cotizador__volver">
                        <img src={Atras} /> <p className="cotizador__volver">volver</p>
                    </div></a>
                    <div className="cotizacion__bienvenida">
                        <p className="cotizacion__bienvenida--nombre">¡Hola, <span>Juan!</span></p>
                        <p className="cotizacion__bienvenida--parrafo">Completa los datos de tu auto</p>
                    </div>
                    <div className="cotizacion__vehiculo">
                        <div>
                            <p className="cotizacion__vehiculo--placa">Placa: C2U-114</p>
                            <p className="cotizacion__vehiculo--marca--modelo">Wolkswagen 2019 Golf</p>
                        </div>
                        <div className="login__img--personal">
                            <img src={Personal} alt="" />
                        </div>
                    </div>
                    <div className="cotizacion__detalle">
                        <div>
                            <p className="cotizacion__detalle--label">Indica la suma asegurada</p>
                            <div className="cotizacion__detalle--montos">
                                <p className="cotizacion__detalle--marca--modelo">MIN $12,500  </p>
                                <p className="cotizacion__detalle--marca--modelo-derecha">MAX $16,500  </p>

                            </div>

                        </div>
                        <div className="cotizacion__data">
                            <input type="text" className="cotizacion__input--text"></input>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Form
import "../../scss/app.scss";
import React from 'react'
import Header from '../../Personas/components/Header';
import Form from './Form';


const CotizadorLayout = () =>
{
    return(
        <>
        <Header />
        <Form />
    </>
    );
}

export default CotizadorLayout